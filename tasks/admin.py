from django.contrib import admin
from tasks.models import Task


# Register your models here.
class TaskAdmin(admin.ModelAdmin):
    fields = ["name", "start_date", "due_date", "is_completed"]


admin.site.register(Task, TaskAdmin)
